# Raytracer

## Building the project

### Dependencies

You need the following to build the project:

* A C++17 capable compiler
* The `autoconf-archive` package installed
* The `yaml-cpp` library installed, see this
  [link](https://github.com/jbeder/yaml-cpp) or your distribution's package
  repositories.

You also need to initialize the submodules in `third-party/` by using:
`git submodule update --init --recursive`

### Build

Use the following command to bootstrap the package, configure the build system,
and build the `raytracer` binary.

```sh
42sh$ export CXXFLAGS="-03 -ffast-math" # Recommended release flags
42sh$ ./bootstrap
42sh$ ./configure # If the NOCONFIGURE environment variable was set on bootstrap
42sh$ make -j4
```

## Usage

You can define the scene to be rendered in a YAML file. Examples are available
in the `example/` directory.
