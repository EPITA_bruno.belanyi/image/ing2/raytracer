#pragma once

#include <utility>

#include "color.hh"
#include "point.hh"

namespace raytracer
{

struct Light
{
    virtual ~Light() = default;

    virtual CalcColor get_illumination(const Position& pos) const = 0;
    virtual std::pair<Vector, double>
    vector_to_light(const Position& pos) const = 0;
};

} // namespace raytracer
