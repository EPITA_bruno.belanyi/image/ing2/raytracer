#pragma once

#include <algorithm>
#include <limits>
#include <utility>

#include "light.hh"

namespace raytracer
{

struct DirectionalLight final : Light
{
    const Vector d;
    const CalcColor c;

    virtual ~DirectionalLight() = default;

    explicit DirectionalLight(Vector dir, CalcColor color = CalcColor{1, 1, 1})
    : d(std::move(dir.normalize())), c(std::move(color))
    {}

    CalcColor get_illumination(const Position& pos) const override
    {
        return c;
    }

    std::pair<Vector, double>
    vector_to_light(const Position& pos) const override
    {
        return {d * -1, std::numeric_limits<double>::max()};
    }
};

} // namespace raytracer
