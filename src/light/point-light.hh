#pragma once

#include <utility>

#include "light.hh"

namespace raytracer
{

struct PointLight final : Light
{
    const Position p;
    const CalcColor c;

    explicit PointLight(Position position, CalcColor color = CalcColor{1, 1, 1})
    : p(position), c(color)
    {}

    virtual ~PointLight() = default;

    CalcColor get_illumination(const Position& pos) const override
    {
        const auto dist = p.dist(pos);
        return c / dist;
    }

    std::pair<Vector, double>
    vector_to_light(const Position& pos) const override
    {
        auto delt = p - pos;
        auto dist = delt.norm();

        return {delt.normalize(), dist} ;
    }
};

} // namespace raytracer
