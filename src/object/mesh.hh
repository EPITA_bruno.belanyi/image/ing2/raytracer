#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "object.hh"
#include "tri.hh"

namespace raytracer
{

template <typename TriImplementation>
struct MeshImpl final : Object
{
    const Texture mat;
    const std::vector<TriImplementation> tris;

    virtual ~MeshImpl() = default;

    explicit MeshImpl(
        Texture material,
        std::vector<TriImplementation> triangles
    ) : mat(material), tris(std::move(triangles))
    {}

    std::optional<double>
    intersect(const Ray& ray) const override
    {
        std::optional<double> t = std::nullopt;
        for (const auto& tri : tris)
            if (const auto dist = tri.intersect(ray); dist)
                if (!t || *dist < *t)
                    t = dist;
        return t;
    }

    Vector normal(const Position& pos) const override
    {
        for (const auto& tri : tris)
            if (tri.contains(pos))
                return tri.normal(pos);
        assert(false);
        return Vector{1, 0, 0};
    }

    const TextureMaterial& texture(const Position& pos) const override
    {
        return mat;
    }
};

using NormalMesh = MeshImpl<NormalTri>;
using CullingMesh = MeshImpl<CullingTri>;
using InterpolMesh = MeshImpl<InterpolTri>;
using InterpolCullingMesh = MeshImpl<InterpolCullingTri>;

} // namespace raytracer
