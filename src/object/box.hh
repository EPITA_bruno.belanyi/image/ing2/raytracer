#pragma once

#include <iosfwd>
#include <memory>

#include <cmath>

#include "aabb.hh"
#include "object.hh"

namespace raytracer
{

struct Box final : Object
{
    const Texture mat;
    const AABB box;

    virtual ~Box() = default;

    explicit Box(
        std::unique_ptr<TextureMaterial> material,
        Position lower_bound,
        Position upper_bound
    ) : mat(std::move(material)), box(lower_bound, upper_bound)
    {}

    std::optional<double>
    intersect(const Ray& ray) const override final
    {
        // See <http://www.cs.utah.edu/~awilliam/box/box.pdf>,
        // "An Efficient and Robust Ray–Box Intersection Algorithm"
        const auto bounds = [&](double val) -> const Position& {
            return val < 0 ? box.u : box.l;
        };

        double tmin = (bounds(ray.inv.x).x - ray.pos.x) * ray.inv.x;
        double tmax = (bounds(-ray.inv.x).x - ray.pos.x) * ray.inv.x;

        const double tymin = (bounds(ray.inv.y).y - ray.pos.y) * ray.inv.y;
        const double tymax = (bounds(-ray.inv.y).y - ray.pos.y) * ray.inv.y;

        if (tmin > tymax || tymin > tmax)
            return std::nullopt;

        if (tymin > tmin)
            tmin = tymin;
        if (tymax < tmax)
            tmax = tymax;

        const double tzmin = (bounds(ray.inv.z).z - ray.pos.z) * ray.inv.z;
        const double tzmax = (bounds(-ray.inv.z).z - ray.pos.z) * ray.inv.z;

        if (tmin > tzmax || tzmin > tmax)
            return std::nullopt;

        if (tzmin > tmin)
            tmin = tzmin;
        if (tzmax < tmax)
            tmax = tzmax;

        if (tmin < 0)
            if (tmax < 0)
                return std::nullopt;
            else
                return tmax;
        else
            return tmin;

    }

    Vector normal(const Position& pos) const override final
    {
        const auto semi_diag = box.diagonal() / 2.0;
        const auto mid = box.l + (semi_diag);
        auto delt = (pos - mid) / semi_diag;
        constexpr auto collapse = [](double& val) {
            if (std::abs(val) - 1 > 1e-6)
                val = 0;
        };

        collapse(delt.x);
        collapse(delt.y);
        collapse(delt.z);
        return delt.normalize();
    }

    const TextureMaterial& texture(const Position&) const override final
    {
        return mat;
    }
};

} // namespace raytracer
