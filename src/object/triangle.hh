#pragma once

#include <cmath>

#include "object.hh"
#include "tri.hh"

namespace raytracer
{

template <typename TriImplementation>
struct TriangleObject final : Object
{
    const Texture mat;
    const TriImplementation tri;

    explicit TriangleObject(
        Texture material,
        TriImplementation triangle
    ) : mat(material), tri(triangle)
    {}

    virtual ~TriangleObject() = default;

    std::optional<double>
    intersect(const Ray& ray) const override
    {
        return tri.intersect(ray);
    }

    Vector normal(const Position& pos) const override
    {
        return tri.normal(pos);
    }

    const TextureMaterial& texture(const Position&) const override final
    {
        return mat;
    }
};

using NormalTriangle = TriangleObject<NormalTri>;
using CullingTriangle = TriangleObject<CullingTri>;
using InterpolTriangle = TriangleObject<InterpolTri>;
using InterpolCullingTriangle = TriangleObject<InterpolCullingTri>;

} // namespace raytracer::detail
