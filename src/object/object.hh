#pragma once

#include <optional>

#include "ray.hh"
#include "material/texture-material.hh"

namespace raytracer
{

struct Object
{
    virtual ~Object() = default;

    virtual std::optional<double>
    intersect(const Ray& ray) const = 0;

    virtual Vector normal(const Position& pos) const = 0;

    virtual const TextureMaterial& texture(const Position& pos) const = 0;
};

} // namespace raytracer
