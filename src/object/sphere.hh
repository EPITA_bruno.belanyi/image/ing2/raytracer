#pragma once

#include <iosfwd>
#include <memory>
#include <utility>

#include <cmath>

#include "material/texture-material.hh"
#include "object.hh"
#include "point.hh"

namespace raytracer
{

struct Sphere final : Object
{
    const Texture mat;
    const Position c;
    const double r;

    explicit Sphere(
        std::unique_ptr<TextureMaterial> material,
        Position center = Position(),
        double radius = 1
    ) : mat(std::move(material)), c(center), r(radius)
    {}

    virtual ~Sphere() = default;

    std::optional<double>
    intersect(const Ray& ray) const override final
    {
        const auto delt = c - ray.pos;
        const auto tca = delt * ray.dir;
        const auto d2 = delt * delt - tca * tca;

        if (d2 > r * r)
            return std::nullopt;

        const auto thc = std::sqrt(r * r - d2);
        auto t_0 = tca - thc;
        auto t_1 = tca + thc;

        if (t_0 > t_1)
            std::swap(t_0, t_1);
        if (t_0 < 0)
            t_0 = t_1;

        if (t_0 < 0)
            return std::nullopt;
        return t_0;
    }

    Vector normal(const Position& pos) const override final
    {
        auto norm = pos - c;
        return norm.normalize();
    }

    const TextureMaterial& texture(const Position&) const override final
    {
        return mat;
    }

    friend std::ostream& operator<<(std::ostream& os, Sphere s)
    {
        return os << "Sphere{c: " << s.c << ", r: " << s.r << '}';
    }
};

} // namespace raytracer
