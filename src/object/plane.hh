#pragma once

#include <iosfwd>
#include <memory>

#include "object.hh"

namespace raytracer
{

struct Plane final : Object
{
    const Texture mat;
    const Position p;
    const Vector n;

    virtual ~Plane() = default;

    explicit Plane(
        std::unique_ptr<TextureMaterial> material,
        Position point,
        Vector normal
    ) : mat(std::move(material)), p(point), n(normal.normalize())
    {}

    std::optional<double>
    intersect(const Ray& ray) const override final
    {
        auto cos = n * ray.dir;
        if (cos > 1e-6) // We want a negative, non-zero value
            return std::nullopt;

        const auto to_orig = p - ray.pos;
        auto t = (to_orig * n) / cos;
        if (t < 0)
            return std::nullopt;
        return t;

    }

    Vector normal(const Position& pos) const override final
    {
        return n;
    }

    const TextureMaterial& texture(const Position&) const override final
    {
        return mat;
    }

    friend std::ostream& operator<<(std::ostream& os, const Plane& plane)
    {
        return os << "Plane{p: " << plane.p << ", n: " << plane.n << '}';
    }
};

} // namespace raytracer
