#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "mesh.hh"
#include "object.hh"
#include "triangle.hh"
#include "detail/blob-frontiers.hh"

namespace raytracer
{

struct Potential
{
    Position pos;
    double val;
};

namespace detail
{
template <bool ShouldInterpolate>
struct BlobBuilderTrait
{
    using MeshType = NormalMesh;
    using TriType = NormalTri;
};

template <>
struct BlobBuilderTrait<true>
{
    using MeshType = InterpolMesh;
    using TriType = InterpolTri;
};
} // namespace detail

template <template <bool> typename Trait, bool ShouldInterpolate>
struct BlobBuilder
{
    BlobBuilder(Texture text_material)
    : material(text_material)
    {}

    std::unique_ptr<typename Trait<ShouldInterpolate>::MeshType> build()
    {
        for (double deltx = 0; deltx < (upper_bound.x - lower_bound.x);
             deltx += step_size)
        {
            for (double delty = 0; delty < (upper_bound.y - lower_bound.y);
                delty += step_size)
            {
                for (double deltz = 0; deltz < (upper_bound.z - lower_bound.z);
                    deltz += step_size)
                {
                    calculate_triangles(Vector{deltx, delty, deltz});
                }
            }
        }

        return std::make_unique<typename Trait<ShouldInterpolate>::MeshType>(
            std::move(material),
            std::move(triangles)
        );
    }

    BlobBuilder& with_material(std::unique_ptr<TextureMaterial> new_mat)
    {
        material = std::move(new_mat);
        return *this;
    }

    BlobBuilder& with_potentials(std::vector<Potential> new_potentials)
    {
        potentials = std::move(new_potentials);
        return *this;
    }

    BlobBuilder& with_step_size(double new_step_size)
    {
        step_size = new_step_size;
        return *this;
    }

    BlobBuilder& with_level_line(double new_level_line)
    {
        level_line = new_level_line;
        return *this;
    }

    BlobBuilder& with_lower_bound(const Position& new_bound)
    {
        lower_bound = new_bound;
        return *this;
    }

    BlobBuilder& with_upper_bound(const Position& new_bound)
    {
        upper_bound = new_bound;
        return *this;
    }

private:
    void calculate_triangles(const Vector& delta)
    {
        // Start at highest corner
        Position corner = upper_bound - delta;
        const Vector dx = Vector{-step_size};
        const Vector dy = Vector{0, -step_size};
        const Vector dz = Vector{0, 0, -step_size};
        double potential_at[8] = {};

        std::size_t index = 0;
        if ((potential_at[0] = potential(corner)) <= level_line)
            index |= 1;
        if ((potential_at[1] = potential(corner + dy)) <= level_line)
            index |= 2;
        if ((potential_at[2] = potential(corner + dx + dy)) <= level_line)
            index |= 4;
        if ((potential_at[3] = potential(corner + dx)) <= level_line)
            index |= 8;
        if ((potential_at[4] = potential(corner + dz)) <= level_line)
            index |= 16;
        if ((potential_at[5] = potential(corner + dy + dz)) <= level_line)
            index |= 32;
        if ((potential_at[6] = potential(corner + dx + dy + dz)) <= level_line)
            index |= 64;
        if ((potential_at[7] = potential(corner + dx + dz)) <= level_line)
            index |= 128;
        add_triangles_from_index(index, corner, potential_at);
    }

    double potential(const Position& at)
    {
        double ans = 0;
        for (const auto& [pos, val] : potentials)
            ans += val / (pos.dist(at) * pos.dist(at));
        return ans;
    }

    void add_triangles_from_index(
        const std::size_t index,
        const Position& upper_corner,
        const double potential_at[8]
    )
    {
        const Vector dx = Vector{-step_size};
        const Vector dy = Vector{0, -step_size};
        const Vector dz = Vector{0, 0, -step_size};
        const auto interpol_between = [&]
            ([[maybe_unused]] const auto lhs, [[maybe_unused]] const auto rhs) {
                if constexpr (ShouldInterpolate)
                {
                    const auto dist = potential_at[rhs] - potential_at[lhs];
                    return (level_line - potential_at[lhs]) / (dist);
                }
                else
                    return 0.5;
        };
        const Position middles[12] =
        {
            upper_corner + dy * interpol_between(0, 1),
            upper_corner + dx * interpol_between(1, 2) + dy,
            upper_corner + dx + dy * interpol_between(3, 2),
            upper_corner + dx * interpol_between(0, 3),
            upper_corner + dy * interpol_between(4, 5) + dz,
            upper_corner + dx * interpol_between(5, 6) + dy + dz,
            upper_corner + dx + dy * interpol_between(7, 6) + dz,
            upper_corner + dx * interpol_between(4, 7) + dz,
            upper_corner + dz * interpol_between(0, 4),
            upper_corner + dy + dz * interpol_between(1, 5),
            upper_corner + dx + dy + dz * interpol_between(2, 6),
            upper_corner + dx + dz * interpol_between(3, 7),
        };
        Vector normals[12];
        for (auto i = 0; i < 12; ++i)
            normals[i] = normal_at(middles[i]);

        for (const auto& [p0, p1, p2] : detail::frontiers[index])
        {
            if (p0 == 255)
                continue;
            if constexpr (ShouldInterpolate)
            {
                triangles.emplace_back(
                    middles[p0], middles[p1], middles[p2],
                    normals[p0], normals[p1], normals[p2]
                );
            }
            else
            {
                triangles.emplace_back(middles[p0], middles[p1], middles[p2]);
            }
        }
    }

    Vector normal_at(const Position& pos)
    {
        const auto d = step_size;
        const auto deltx = Vector{step_size / 2};
        const auto delty = Vector{0, step_size / 2};
        const auto deltz = Vector{0, 0, step_size / 2};
        // Opposite of gradient is the normal of our iso-surface
        const auto dx = (potential(pos - deltx) - potential(pos + deltx)) / d;
        const auto dy = (potential(pos - delty) - potential(pos + delty)) / d;
        const auto dz = (potential(pos - deltz) - potential(pos + deltz)) / d;
        return Vector{dx, dy, dz}.normalize();
    }

    std::vector<
        typename Trait<ShouldInterpolate>::TriType
    > triangles;

    Texture material;
    std::vector<Potential> potentials;
    double step_size;
    double level_line;
    Position lower_bound;
    Position upper_bound;
};

using NormalBlobBuilder = BlobBuilder<detail::BlobBuilderTrait, false>;
using InterpolBlobBuilder = BlobBuilder<detail::BlobBuilderTrait, true>;

} // namespace raytracer
