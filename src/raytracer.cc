#include <fstream>
#include <iostream>
#include <string>

#include <boost/program_options.hpp>

#include "scene/scene-parser.hh"

int main(int argc, char** argv)
{
    using namespace boost::program_options;
    using namespace raytracer;

    try
    {
        options_description desc{"Options"};
        desc.add_options()
            ("help,h", "Usage")
            ("input,i",
             value<std::string>()->default_value("scene.yaml"),
             "Output file")
            ("output,o",
             value<std::string>()->default_value("scene.ppm"),
             "Input file");

        variables_map vm;
        store(parse_command_line(argc, argv, desc), vm);
        notify(vm);

        if (vm.count("help"))
            std::cout << desc << '\n';
        else
        {
            auto scene = SceneParser::parse_scene(
                vm["input"].as<std::string>()
            );
            auto output = std::ofstream(vm["output"].as<std::string>());
            scene.render(output);
        }
    }
    catch (const error &ex)
    {
        std::cerr << ex.what() << '\n';
    }
}
