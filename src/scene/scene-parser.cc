#include <fstream>
#include <memory>
#include <string>
#include <vector>

#include <cassert>

#include <yaml-cpp/yaml.h>

#include "scene-parser.hh"

#include "color.hh"
#include "point.hh"
#include "vect.hh"
#include "light/directional-light.hh"
#include "light/point-light.hh"
#include "material/uniform-material.hh"
#include "object/box.hh"
#include "object/blob.hh"
#include "object/mesh.hh"
#include "object/plane.hh"
#include "object/sphere.hh"
#include "object/triangle.hh"
#include "scene/scene-builder.hh"
#include "scene/scene-parser.hh"

using light_type = std::unique_ptr<raytracer::Light>;
using object_type = std::unique_ptr<raytracer::Object>;
using texture_type = std::unique_ptr<raytracer::TextureMaterial>;

namespace YAML
{

template <>
struct convert<object_type>
{
    static Node encode(const object_type& rhs)
    {
        Node node;
        return node;
    }


    static bool decode(const Node& node, object_type& rhs)
    {
        using namespace raytracer;
        if(!node.IsMap())
            return false;
        if (node["type"].as<std::string>() == "sphere")
            rhs.reset(new Sphere{
                node["texture"].as<texture_type>(),
                node["position"].as<Position>(),
                node["radius"].as<double>(),
            });
        else if (node["type"].as<std::string>() == "plane")
            rhs.reset(new Plane{
                node["texture"].as<texture_type>(),
                node["position"].as<Position>(),
                node["normal"].as<Vector>(),
            });
        else if (node["type"].as<std::string>() == "box")
            rhs.reset(new Box{
                node["texture"].as<texture_type>(),
                node["lower_bound"].as<Position>(),
                node["upper_bound"].as<Position>(),
            });
        else if (node["type"].as<std::string>() == "triangle")
            rhs.reset(new NormalTriangle{
                node["texture"].as<texture_type>(),
                node["corners"].as<NormalTri>(),
            });
        else if (node["type"].as<std::string>() == "culling-triangle")
            rhs.reset(new CullingTriangle{
                node["texture"].as<texture_type>(),
                node["corners"].as<CullingTri>(),
            });
        else if (node["type"].as<std::string>() == "interpolating-triangle")
            rhs.reset(new InterpolTriangle{
                node["texture"].as<texture_type>(),
                node["corners"].as<InterpolTri>(),
            });
        else if (node["type"].as<std::string>() == "interpol-culling-triangle")
            rhs.reset(new InterpolCullingTriangle{
                node["texture"].as<texture_type>(),
                node["corners"].as<InterpolCullingTri>(),
            });
        else if (node["type"].as<std::string>() == "mesh")
            rhs.reset(new NormalMesh{
                node["texture"].as<texture_type>(),
                node["triangles"].as<std::vector<NormalTri>>(),
            });
        else if (node["type"].as<std::string>() == "culling-mesh")
            rhs.reset(new CullingMesh{
                node["texture"].as<texture_type>(),
                node["triangles"].as<std::vector<CullingTri>>(),
            });
        else if (node["type"].as<std::string>() == "interpolating-mesh")
            rhs.reset(new InterpolMesh{
                node["texture"].as<texture_type>(),
                node["triangles"].as<std::vector<InterpolTri>>(),
            });
        else if (node["type"].as<std::string>() == "interpol-culling-mesh")
            rhs.reset(new InterpolCullingMesh{
                node["texture"].as<texture_type>(),
                node["triangles"].as<std::vector<InterpolCullingTri>>(),
            });
        else if (node["type"].as<std::string>() == "spiky-blob")
            rhs = NormalBlobBuilder{node["texture"].as<texture_type>()}
                .with_potentials(node["potentials"].as<std::vector<Potential>>())
                .with_step_size(node["step_size"].as<double>())
                .with_level_line(node["level_line"].as<double>())
                .with_lower_bound(node["lower_bound"].as<Position>())
                .with_upper_bound(node["upper_bound"].as<Position>())
                .build();
        else if (node["type"].as<std::string>() == "blob")
            rhs = InterpolBlobBuilder{node["texture"].as<texture_type>()}
                .with_potentials(node["potentials"].as<std::vector<Potential>>())
                .with_step_size(node["step_size"].as<double>())
                .with_level_line(node["level_line"].as<double>())
                .with_lower_bound(node["lower_bound"].as<Position>())
                .with_upper_bound(node["upper_bound"].as<Position>())
                .build();
        else
        {
            assert(false);
        }
        return true;
    }
};

template <>
struct convert<light_type>
{
    static Node encode(const light_type& rhs)
    {
        using namespace raytracer;
        Node node;
        if (const auto* dir = dynamic_cast<const DirectionalLight*>(rhs.get()))
        {
            node["type"] = "directional";
            node["direction"] = dir->d;
            node["color"] = dir->c;
        }
        else if (const auto* point = dynamic_cast<const PointLight*>(rhs.get()))
        {
            node["type"] = "point";
            node["position"] = point->p;
            node["color"] = point->c;
        }
        else
        {
            assert(false);
        }
        return node;
    }


    static bool decode(const Node& node, light_type& rhs)
    {
        using namespace raytracer;
        if(!node.IsMap())
            return false;

        if (node["type"].as<std::string>() == "point")
            rhs.reset(new PointLight{
                node["position"].as<Position>(),
                node["color"].as<CalcColor>(),
            });
        else if (node["type"].as<std::string>() == "directional")
            rhs.reset(new DirectionalLight{
                node["direction"].as<Vector>(),
                node["color"].as<CalcColor>(),
            });
        else
            return false;

        return true;
    }
};

template<>
struct convert<const ::raytracer::TextureMaterial*>
{
    static Node encode(const ::raytracer::TextureMaterial* const& rhs)
    {
        using namespace raytracer;
        Node node;
        if (const auto* uni = dynamic_cast<const UniformMaterial*>(rhs))
        {
            node["type"] = "uniform";
            node["diffuse"] = uni->k_d;
            node["specular"] = uni->k_s;
            node["reflectivity"] = uni->r;
        }
        else
        {
            assert(false);
        }
        return node;
    }


    static bool decode(const Node& node, ::raytracer::TextureMaterial*& rhs)
    {
        using namespace raytracer;
        if(!node.IsMap())
            return false;

        if (node["type"].as<std::string>() == "uniform")
            rhs = new UniformMaterial{
                node["diffuse"].as<CalcColor>(),
                node["specular"].as<CalcColor>(),
                node["reflectivity"].as<double>(),
            };
        else
            return false;

        return true;
    }
};

template <>
struct convert<texture_type>
{
    static Node encode(const std::unique_ptr<::raytracer::TextureMaterial>& rhs)
    {
        return convert<const ::raytracer::TextureMaterial*>::encode(rhs.get());
    }


    static bool decode(const Node& node, texture_type& rhs)
    {
        ::raytracer::TextureMaterial* tmp = nullptr;
        auto ret = convert<const ::raytracer::TextureMaterial*>::decode(
            node,
            tmp
        );
        rhs.reset(tmp);
        return ret;
    }
};

template <>
struct convert<::raytracer::Potential>
{
    static Node encode(const ::raytracer::Potential& rhs)
    {
        Node node;
        node["position"] = rhs.pos;
        node["value"] = rhs.val;
        return node;
    }


    static bool decode(const Node& node, ::raytracer::Potential& rhs)
    {
        if(!node.IsMap() || node.size() != 2)
            return false;

        rhs.pos = node["position"].as<::raytracer::Position>();
        rhs.val = node["value"].as<double>();

        return true;
    }
};

template <>
struct convert<::raytracer::CalcColor>
{
    static Node encode(const ::raytracer::CalcColor& rhs)
    {
        Node node;
        node.push_back(rhs.r);
        node.push_back(rhs.g);
        node.push_back(rhs.b);
        return node;
    }


    static bool decode(const Node& node, ::raytracer::CalcColor& rhs)
    {
        if(!node.IsSequence() || node.size() != 3)
            return false;

        rhs.r = node[0].as<double>();
        rhs.g = node[1].as<double>();
        rhs.b = node[2].as<double>();

        return true;
    }
};

template <>
struct convert<::raytracer::Position>
{
    static Node encode(const ::raytracer::Position& rhs)
    {
        Node node;
        node.push_back(rhs.x);
        node.push_back(rhs.y);
        node.push_back(rhs.z);
        return node;
    }


    static bool decode(const Node& node, ::raytracer::Position& rhs)
    {
        if(!node.IsSequence() || node.size() != 3)
            return false;

        rhs.x = node[0].as<double>();
        rhs.y = node[1].as<double>();
        rhs.z = node[2].as<double>();

        return true;
    }
};

template <>
struct convert<::raytracer::Vector>
{
    static Node encode(const ::raytracer::Vector& rhs)
    {
        Node node;
        node.push_back(rhs.x);
        node.push_back(rhs.y);
        node.push_back(rhs.z);
        return node;
    }


    static bool decode(const Node& node, ::raytracer::Vector& rhs)
    {
        if(!node.IsSequence() || node.size() != 3)
            return false;

        rhs.x = node[0].as<double>();
        rhs.y = node[1].as<double>();
        rhs.z = node[2].as<double>();

        return true;
    }
};

template <>
struct convert<::raytracer::NormalTri>
{
    static Node encode(const ::raytracer::NormalTri& rhs)
    {
        Node node;
        node["p0"] = rhs.c0;
        node["p1"] = rhs.c0 + rhs.c0c1;
        node["p1"] = rhs.c0 + rhs.c0c2;
        return node;
    }


    static bool decode(const Node& node, ::raytracer::NormalTri& rhs)
    {
        if(!node.IsMap() || node.size() != 3)
            return false;

        rhs.c0 = node["p0"].as<::raytracer::Position>();
        rhs.c0c1 = node["p1"].as<::raytracer::Position>() - rhs.c0;
        rhs.c0c2 = node["p2"].as<::raytracer::Position>() - rhs.c0;

        return true;
    }

};

template <>
struct convert<::raytracer::CullingTri>
{
    static Node encode(const ::raytracer::CullingTri& rhs)
    {
        Node node;
        node["p0"] = rhs.c0;
        node["p1"] = rhs.c0 + rhs.c0c1;
        node["p1"] = rhs.c0 + rhs.c0c2;
        return node;
    }


    static bool decode(const Node& node, ::raytracer::CullingTri& rhs)
    {
        if(!node.IsMap() || node.size() != 3)
            return false;

        rhs.c0 = node["p0"].as<::raytracer::Position>();
        rhs.c0c1 = node["p1"].as<::raytracer::Position>() - rhs.c0;
        rhs.c0c2 = node["p2"].as<::raytracer::Position>() - rhs.c0;

        return true;
    }
};

template <>
struct convert<::raytracer::InterpolTri>
{
    static Node encode(const ::raytracer::InterpolTri& rhs)
    {
        Node node;
        node["p0"] = rhs.c0;
        node["p1"] = rhs.c0 + rhs.c0c1;
        node["p1"] = rhs.c0 + rhs.c0c2;
        node["n0"] = rhs.n0;
        node["n1"] = rhs.n1;
        node["n2"] = rhs.n2;
        return node;
    }


    static bool decode(const Node& node, ::raytracer::InterpolTri& rhs)
    {
        if(!node.IsMap() || node.size() != 6)
            return false;

        rhs.c0 = node["p0"].as<::raytracer::Position>();
        rhs.c0c1 = node["p1"].as<::raytracer::Position>() - rhs.c0;
        rhs.c0c2 = node["p2"].as<::raytracer::Position>() - rhs.c0;
        rhs.n0 = node["n0"].as<::raytracer::Vector>();
        rhs.n1 = node["n1"].as<::raytracer::Vector>();
        rhs.n2 = node["n2"].as<::raytracer::Vector>();

        return true;
    }
};

template <>
struct convert<::raytracer::InterpolCullingTri>
{
    static Node encode(const ::raytracer::InterpolCullingTri& rhs)
    {
        Node node;
        node["p0"] = rhs.c0;
        node["p1"] = rhs.c0 + rhs.c0c1;
        node["p1"] = rhs.c0 + rhs.c0c2;
        node["n0"] = rhs.n0;
        node["n1"] = rhs.n1;
        node["n2"] = rhs.n2;
        return node;
    }


    static bool decode(const Node& node, ::raytracer::InterpolCullingTri& rhs)
    {
        if(!node.IsMap() || node.size() != 6)
            return false;

        rhs.c0 = node["p0"].as<::raytracer::Position>();
        rhs.c0c1 = node["p1"].as<::raytracer::Position>() - rhs.c0;
        rhs.c0c2 = node["p2"].as<::raytracer::Position>() - rhs.c0;
        rhs.n0 = node["n0"].as<::raytracer::Vector>();
        rhs.n1 = node["n1"].as<::raytracer::Vector>();
        rhs.n2 = node["n2"].as<::raytracer::Vector>();

        return true;
    }
};

} // namespace YAML

namespace raytracer
{

Scene SceneParser::parse_scene(std::istream& input)
{
    auto parser = SceneParser{};
    auto scene = YAML::Load(input);
    parser.parse_scene(scene["scene"]);
    return parser.builder_.build();
}

Scene SceneParser::parse_scene(const std::string& filename)
{
    auto input = std::ifstream(filename);
    return parse_scene(input);
}

void SceneParser::parse_scene(const YAML::Node& scene)
{
    if (const auto y = scene["height"])
        builder_.with_y(y.as<unsigned>());
    if (const auto x = scene["width"])
        builder_.with_x(x.as<unsigned>());
    if (const auto count = scene["anti_aliasing_count"])
        builder_.with_anti_aliasing_count(count.as<unsigned>());
    if (const auto limit = scene["recursion_limit"])
        builder_.with_recursion_limit(limit.as<unsigned>());
    if (const auto camera = scene["camera"])
        parse_camera(camera);
    if (const auto lights = scene["lights"])
        for (const auto& light : lights)
            builder_.add_light(light.as<light_type>());
    if (const auto objects = scene["objects"])
        for (const auto& object : objects)
            builder_.add_object(object.as<object_type>());
}

void SceneParser::parse_camera(const YAML::Node& camera)
{
    if (const auto pos = camera["position"])
        builder_.with_camera_position(pos.as<Position>());
    if (const auto fwd = camera["forward"])
        builder_.with_forward(fwd.as<Vector>());
    if (const auto up = camera["up"])
        builder_.with_up(up.as<Vector>());
    if (const auto fov = camera["fov"])
        builder_.with_fov_deg(fov.as<double>());
    if (const auto dist = camera["distance"])
        builder_.with_distance_to_image(dist.as<double>());
}

} // namespace raytracer
