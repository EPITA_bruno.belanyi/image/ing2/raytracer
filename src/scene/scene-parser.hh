#pragma once

#include <istream>
#include <string>

#include <yaml-cpp/yaml.h>

#include "color.hh"
#include "point.hh"
#include "vect.hh"

#include "scene.hh"
#include "scene-builder.hh"

namespace raytracer
{

struct SceneParser
{
    static Scene parse_scene(std::istream& input);

    static Scene parse_scene(const std::string& filename);

private:
    SceneBuilder builder_{};

    void parse_scene(const YAML::Node& scene);

    void parse_camera(const YAML::Node& camera);
};

} // namespace raytracer
