#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "scene.hh"

namespace raytracer
{

struct SceneBuilder
{
    explicit SceneBuilder() = default;

    SceneBuilder(unsigned x_dimension, unsigned y_dimension)
    : x(x_dimension), y(y_dimension)
    {}

    Scene build()
    {
        return Scene{
            Camera{camera_position, forward, up, fov, dist_to_image, x, y},
            Image{x, y},
            std::move(objects),
            std::move(lights),
            anti_aliasing_count,
            recursion_limit,
        };
    }

    SceneBuilder& add_object(std::unique_ptr<const Object> object)
    {
        objects.emplace_back(std::move(object));
        return *this;
    }

    SceneBuilder& add_light(std::unique_ptr<const Light> light)
    {
        lights.emplace_back(std::move(light));
        return *this;
    }

    SceneBuilder& with_camera_position(Position p)
    {
        camera_position = p;
        return *this;
    }

    SceneBuilder& with_up(Vector v)
    {
        up = v;
        return *this;
    }

    SceneBuilder& with_forward(Vector v)
    {
        forward = v;
        return *this;
    }

    SceneBuilder& with_fov(double angle)
    {
        fov = angle;
        return *this;
    }

    SceneBuilder& with_fov_deg(double angle)
    {
        fov = angle / 180 * atan2(0, -1);
        return *this;
    }

    SceneBuilder& with_distance_to_image(double d)
    {
        dist_to_image = d;
        return *this;
    }

    SceneBuilder& with_x(unsigned new_x)
    {
        x = new_x;
        return *this;
    }

    SceneBuilder& with_y(unsigned new_y)
    {
        y = new_y;
        return *this;
    }

    SceneBuilder& with_anti_aliasing_count(unsigned new_count)
    {
        anti_aliasing_count = new_count;
        return *this;
    }

    SceneBuilder& with_recursion_limit(unsigned new_limit)
    {
        recursion_limit = new_limit;
        return *this;
    }

private:
    Position camera_position{0, 0, 0};
    Vector forward{1, 0, 0};
    Vector up{0, 1, 0};
    double fov = atan(1);
    double dist_to_image = 1;
    unsigned x = 1280;
    unsigned y = 720;
    std::vector<std::unique_ptr<const Object>> objects;
    std::vector<std::unique_ptr<const Light>> lights;
    unsigned anti_aliasing_count = 0;
    unsigned recursion_limit = 0;
};

} // namespace raytracer
