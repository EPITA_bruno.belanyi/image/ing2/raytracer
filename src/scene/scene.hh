#pragma once

#include <atomic>
#include <chrono>
#include <functional>
#include <memory>
#include <optional>
#include <ostream>
#include <random>
#include <thread>
#include <utility>
#include <vector>

#include <tbb/parallel_for.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#include <indicators/block_progress_bar.hpp>
#pragma GCC diagnostic pop

#include "camera.hh"
#include "color.hh"
#include "image.hh"
#include "ray.hh"
#include "light/light.hh"
#include "object/object.hh"

namespace raytracer
{

namespace detail
{

inline constexpr Vector reflected(const Vector& ray, const Vector& normal)
{
    const auto proj = ray * normal;
    const auto delt = normal * (2 * proj);
    return ray - delt;
}

} // namespace detail

struct Scene
{
    const Camera camera;
    Image image;
    const std::vector<std::unique_ptr<const Object>> objects;
    const std::vector<std::unique_ptr<const Light>> lights;
    const unsigned anti_aliasing_count;
    const unsigned limit;
    std::atomic<std::size_t> counter = 0;

    std::optional<
        std::pair<double, std::reference_wrapper<const Object>>
    >
    cast_ray(const Position& pos, const Vector& dir) const
    {
        std::optional<std::reference_wrapper<const Object>> shot_object;
        auto t = std::numeric_limits<double>::max();

        for (const auto& obj : objects)
            if (const auto dist = obj->intersect(Ray(pos, dir)); dist && *dist < t)
            {
                t = *dist;
                shot_object = *obj;
            }

        if (shot_object)
            return std::pair{t, *shot_object};
        return std::nullopt;
    }

    void render(std::ostream& output)
    {

        const auto size = image.height() * image.width();
        std::thread progress(
            [&, size=size](){
                using namespace indicators;
                BlockProgressBar bar{
                    option::BarWidth{50},
                    option::Start{" ["},
                    option::End{"]"},
                    option::PrefixText{"Rendering"},
                    option::ShowElapsedTime{true},
                    option::ShowRemainingTime{true},
                };
                while (true)
                {
                    const std::size_t count = counter;
                    bar.set_progress(100 * count / size);
                    std::this_thread::sleep_for(std::chrono::milliseconds(50));
                    if (count == size)
                        break;
                }
                bar.mark_as_completed();
            }
        );
        tbb::parallel_for(
            0u,
            image.height(),
            [&](unsigned y) { render_line(y); }
        );

        progress.join();

        output << image;
    }

private:
    void render_line(const unsigned y)
    {
        for (unsigned x = 0; x < image.width(); ++x)
        {
            if (anti_aliasing_count)
                image.put(x, y, anti_alias_pixel(x, y));
            else
                image.put(x, y, pixel(x, y));
            ++counter;
        }
    }

    CalcColor anti_alias_pixel(const unsigned x, const unsigned y)
    {
        CalcColor ans{};

        static std::random_device rd;
        static std::mt19937 gen(rd());
        static std::uniform_real_distribution<> dis(-0.5, 0.5);

        for (unsigned count = 0; count < anti_aliasing_count; ++count)
        {
            auto x_rand = x + dis(gen);
            auto y_rand = y + dis(gen);

            ans.raw_add(pixel(x_rand, y_rand));
        }

        return ans / anti_aliasing_count;
    }
    CalcColor pixel(double x, double y)
    {
        x /= image.width();
        y /= image.height();

        const auto pix = camera.get_pixel_pos(x, y);
        const auto dir = (pix - camera.p).normalize();

        if (const auto res = cast_ray(pix, dir))
        {
            const auto& shot_pos = pix + dir * res->first;
            return color_at(shot_pos, dir, res->second.get(), limit);
        }
        return CalcColor{};
    }

    CalcColor color_at(
        const Position& pos,
        const Vector& dir,
        const Object& obj,
        unsigned recurse_limit
    ) const
    {
        CalcColor acc{};
        const auto& texture = obj.texture(pos);
        const auto& reflected = detail::reflected(dir, obj.normal(pos));
        const auto& k_d = texture.get_diffuse(pos);
        const auto& k_s = texture.get_specular(pos);

        for (const auto& light : lights)
        {
            const auto& [light_dir, dist_light] = light->vector_to_light(pos);
            // Handle shadows
            if (const auto res = cast_ray(pos + light_dir * 0.001, light_dir))
                if (res->first < dist_light)
                continue;
            const auto& lum = light->get_illumination(pos);

            const auto& diffused = k_d * (obj.normal(pos) * light_dir);
            const auto& specular = k_s * (reflected * light_dir);
            auto diffused_light = lum * diffused;
            auto specular_light = lum * specular;
            const auto& i = diffused_light.clamp_color()
                + specular_light.clamp_color();

            acc += i;
        }
        // Handle reflectivity
        const auto& r = texture.get_reflectivity(pos);
        if (r > 1e-6 && recurse_limit)
        {
            const auto& refl_pos = pos + reflected * 0.001;
            if (const auto res = cast_ray(refl_pos, reflected))
            {
                const auto& shot_pos = refl_pos + reflected * res->first;
                acc += r * color_at(
                    shot_pos,
                    reflected,
                    res->second.get(),
                    recurse_limit - 1
                );
            }

        }

        return acc;
    }
};

} // namespace raytracer
