#pragma once

#include <tuple>

#include "color.hh"
#include "vect.hh"
#include "texture-material.hh"

namespace raytracer
{

struct UniformMaterial final : TextureMaterial
{
    const CalcColor k_d;
    const CalcColor k_s;
    const double r;

    explicit UniformMaterial(
        CalcColor diffuse = CalcColor(),
        CalcColor specular = CalcColor(),
        double reflectivity = 0
    ) : k_d(diffuse), k_s(specular), r(reflectivity)
    {}

    virtual ~UniformMaterial() = default;

    CalcColor get_diffuse() const
    {
        return k_d;
    }

    CalcColor get_specular() const
    {
        return k_s;
    }

    CalcColor get_diffuse(const Position&) const override final
    {
        return get_diffuse();
    }

    CalcColor get_specular(const Position&) const override final
    {
        return get_specular();
    }

    double get_reflectivity(const Position&) const override final
    {
        return r;
    }

    bool cmp(const TextureMaterial& rhs) const override final
    {
        if (const auto* rhs_same = dynamic_cast<const UniformMaterial*>(&rhs))
        {
            return std::tie(k_d, k_s, r)
                < std::tie(rhs_same->k_d, rhs_same->k_s, rhs_same->r);
        }
        // Should not happen
        return false;
    }
};

} // namespace raytracer
