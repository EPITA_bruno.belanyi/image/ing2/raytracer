#pragma once

#include <memory>
#include <set>

#include "color.hh"
#include "point.hh"

namespace raytracer
{

struct TextureMaterial
{
    virtual ~TextureMaterial() = default;

    virtual CalcColor get_diffuse(const Position& pos) const = 0;
    virtual CalcColor get_specular(const Position& pos) const = 0;
    virtual double get_reflectivity(const Position& pos) const = 0;
    virtual bool cmp(const TextureMaterial& rhs) const = 0;
};

inline bool operator<(const TextureMaterial& lhs, const TextureMaterial& rhs)
{
    const auto& lhs_id = typeid(lhs);
    const auto& rhs_id = typeid(rhs);
    if (lhs_id != rhs_id)
        return lhs_id.before(rhs_id);
    return lhs.cmp(rhs);
}

struct Texture
{
    Texture(std::unique_ptr<TextureMaterial> val)
    : texture_(instances_.insert(std::move(val)).first->get())
    {}

    operator const TextureMaterial&() const
    {
        return *texture_;
    }

private:
    const TextureMaterial* texture_;

    struct cmp_ptr
    {
        using ptr_type = std::unique_ptr<const TextureMaterial>;
        bool operator()(const ptr_type& lhs, const ptr_type& rhs) const
        {
            return *lhs < *rhs;
        }
    };

    inline static std::set<
        std::unique_ptr<const TextureMaterial>,
        cmp_ptr
    > instances_{};
};

} // namespace raytracer
