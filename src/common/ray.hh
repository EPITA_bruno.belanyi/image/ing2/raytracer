#pragma once

#include "point.hh"
#include "vect.hh"

namespace raytracer
{

struct Ray
{
    const Position pos;
    const Vector dir;
    const Vector inv;

    Ray(Position position, Vector direction)
    : pos(position), dir(direction), inv(1.0 / direction)
    {}
};

} // namespace raytracer
