#pragma once

#include <iosfwd>
#include <utility>
#include <tuple>
#include <type_traits>

#include <cassert>

#include "vect.hh"

namespace raytracer
{

template <typename T>
struct Point
{
    T x;
    T y;
    T z;

    template <typename U>
    friend constexpr Point<decltype(std::declval<T>() + std::declval<U>())>
    operator+(const Point<T>& lhs, const Vect<U>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());

        return Point<Result>{
            lhs.x + rhs.x,
            lhs.y + rhs.y,
            lhs.z + rhs.z
        };
    }

    template <typename U>
    friend constexpr Point<decltype(std::declval<T>() + std::declval<U>())>
    operator+(const Vect<U>& lhs, const Point<T>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());

        return Point<Result>{
            rhs.x + lhs.x,
            rhs.y + lhs.y,
            rhs.z + lhs.z
        };
    }

    template <typename U>
    friend constexpr Point<decltype(std::declval<T>() - std::declval<U>())>
    operator-(const Point<T>& lhs, const Vect<U>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());

        return Point<Result>{
            lhs.x - rhs.x,
            lhs.y - rhs.y,
            lhs.z - rhs.z
        };
    }

    template <typename U>
    friend constexpr Point<decltype(std::declval<T>() - std::declval<U>())>
    operator-(const Vect<U>& lhs, const Point<T>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());

        return Point<Result>{
            lhs.x - rhs.x,
            lhs.y - rhs.y,
            lhs.z - rhs.z
        };
    }

    template <typename U>
    friend constexpr Vect<decltype(std::declval<T>() - std::declval<U>())>
    operator-(const Point<T>& lhs, const Point<U>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());

        return Vect<Result>{
            lhs.x - rhs.x,
            lhs.y - rhs.y,
            lhs.z - rhs.z,
        };
    }

    friend std::ostream& operator<<(std::ostream& s, const Point<T>& p)
    {
        return s << "Point{x: " << p.x << " y: " << p.y << " z: " << p.z << '}';
    }


    double norm() const
    {
        return std::sqrt(x * x + y * y + z * z);
    }

    template <typename U>
    double dist(const Point<U>& other) const
    {
        return (*this - other).norm();
    }

    friend bool operator<(const Point<T>& lhs, const Point<T>& rhs)
    {
        return std::tie(lhs.x, lhs.y, lhs.z) < std::tie(rhs.x, rhs.y, rhs.z);
    }
};

using Position = Point<double>;

} // namespace raytracer
