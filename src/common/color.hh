#pragma once

#include <iosfwd>
#include <utility>
#include <tuple>
#include <type_traits>

namespace raytracer
{

namespace detail
{

template <typename T>
constexpr T clamp(T v, T min = 0, T max = 1)
{
    if (v >= min)
        if (v <= max)
            return v;
        else
            return max;
    else
        return min;
}

} // namespace detail

template <typename T>
struct Color;

template <>
struct Color<unsigned char>
{
    unsigned char r;
    unsigned char g;
    unsigned char b;

    Color(unsigned char  red = 0, unsigned char green = 0, unsigned blue = 0)
    : r(red), g(green), b(blue)
    {}

    template <typename U>
    friend constexpr
    Color<decltype(std::declval<unsigned char>() * std::declval<U>())>
    operator*(const Color<unsigned char>& lhs, const Color<U>& rhs)
    {
        using Result = decltype(
                std::declval<unsigned char>() * std::declval<U>()
        );
        return Color<Result>{
            lhs.r * rhs.r / 255,
            lhs.g * rhs.g / 255,
            lhs.b * rhs.b / 255
        };
    }

    template <typename U>
    friend constexpr
    Color<decltype(std::declval<unsigned char>() / std::declval<U>())>
    operator/(const Color<unsigned char>& lhs, const Color<U>& rhs)
    {
        using Result = decltype(
                std::declval<unsigned char>() / std::declval<U>()
        );
        return Color<Result>{
            lhs.r / rhs.r,
            lhs.g / rhs.g,
            lhs.b / rhs.b
        };
    }

    template <typename U>
    friend constexpr std::enable_if_t<
        std::is_arithmetic_v<U>,
        Color<decltype(std::declval<unsigned char>() * std::declval<U>())>
    >
    operator*(const Color<unsigned char>& lhs, U rhs)
    {
        using Result = decltype(
                std::declval<unsigned char>() * std::declval<U>()
        );
        return Color<Result>{
            lhs.r * rhs / 255,
            lhs.g * rhs / 255,
            lhs.b * rhs / 255
        };
    }

    template <typename U>
    friend constexpr std::enable_if_t<
        std::is_arithmetic_v<U>,
        Color<decltype(std::declval<unsigned char>() * std::declval<U>())>
    >
    operator*(U lhs, const Color<unsigned char>& rhs)
    {
        using Result = decltype(
                std::declval<unsigned char>() * std::declval<U>()
        );
        return Color<Result>{
            rhs.r * lhs / 255,
            rhs.g * lhs / 255,
            rhs.b * lhs / 255
        };
    }

    template <typename U>
    friend constexpr std::enable_if_t<
        std::is_arithmetic_v<U>,
        Color<decltype(std::declval<unsigned char>() / std::declval<U>())>
    >
    operator/(const Color<unsigned char>& lhs, U rhs)
    {
        using Result = decltype(
            std::declval<unsigned char>() / std::declval<U>()
        );
        return Color<Result>{lhs.r / rhs, lhs.g / rhs, lhs.b / rhs};
    }

    template <typename U>
    friend constexpr std::enable_if_t<
        std::is_arithmetic_v<U>,
        Color<decltype(std::declval<unsigned char>() / std::declval<U>())>
    >
    operator/(U lhs, const Color<unsigned char>& rhs)
    {
        using Result = decltype(
            std::declval<unsigned char>() / std::declval<U>()
        );
        return Color<Result>{lhs / rhs.r, lhs / rhs.g, lhs / rhs.b};
    }


    friend std::ostream&
    operator<<(std::ostream& s, const Color<unsigned char>& c)
    {
        return s << "Color<unsigned char>{r: " << c.r
                 << ", g: " << c.g
                 << ", b: " << c.b << '}';
    }
};

template <typename T>
struct Color
{
    T r;
    T g;
    T b;

    Color(
        std::enable_if_t<!std::is_integral_v<T>, T> red = 0,
        T green = 0,
        T blue = 0
    ) : r(detail::clamp(red)), g(detail::clamp(green)), b(detail::clamp(blue))
    {}

    Color& raw_add(const Color& rhs)
    {
        r += rhs.r;
        g += rhs.g;
        b += rhs.b;
        return *this;
    }

    Color& clamp_color()
    {
        using namespace detail;
        r = clamp(r);
        g = clamp(g);
        b = clamp(b);
        return *this;
    }

    Color& operator+=(const Color& rhs)
    {
        using namespace detail;
        r = clamp(r + rhs.r);
        g = clamp(g + rhs.g);
        b = clamp(b + rhs.b);
        return *this;
    }

    friend constexpr Color
    operator+(const Color& lhs, const Color& rhs)
    {
        using namespace detail;
        return Color{
            clamp(lhs.r + rhs.r),
            clamp(lhs.g + rhs.g),
            clamp(lhs.b + rhs.b),
        };
    }

    template <typename U>
    friend constexpr Color<decltype(std::declval<T>() * std::declval<U>())>
    operator*(const Color<T>& lhs, const Color<U>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());
        return Color<Result>{lhs.r * rhs.r, lhs.g * rhs.g, lhs.b * rhs.b};
    }

    template <typename U>
    friend constexpr Color<decltype(std::declval<T>() / std::declval<U>())>
    operator/(const Color<T>& lhs, const Color<U>& rhs)
    {
        using Result = decltype(std::declval<T>() / std::declval<U>());
        return Color<Result>{lhs.r / rhs.r, lhs.g / rhs.g, lhs.b / rhs.b};
    }

    friend constexpr Color<T> operator*(const Color<T>& lhs, T rhs)
    {
        return Color<T>{lhs.r * rhs, lhs.g * rhs, lhs.b * rhs};
    }

    friend constexpr Color<T> operator*(T lhs, const Color<T>& rhs)
    {
        return Color<T>{rhs.r * lhs, rhs.g * lhs, rhs.b * lhs};
    }

    friend constexpr Color<T> operator/(const Color<T>& lhs, T rhs)
    {
        return Color<T>{lhs.r / rhs, lhs.g / rhs, lhs.b / rhs};
    }

    friend constexpr Color<T> operator/(T lhs, const Color<T>& rhs)
    {
        return Color<T>{rhs.r / lhs, rhs.g / lhs, rhs.b / lhs};
    }

    operator Color<unsigned char>() const
    {
        using namespace detail;
        return Color<unsigned char>{
            static_cast<unsigned char>(clamp(r) * 255),
            static_cast<unsigned char>(clamp(g) * 255),
            static_cast<unsigned char>(clamp(b) * 255)
        };
    }

    friend bool operator<(const Color<T>& lhs, const Color<T>& rhs)
    {
        return std::tie(lhs.r, lhs.g, lhs.b) < std::tie(rhs.r, rhs.g, rhs.b);
    }

    friend std::ostream& operator<<(std::ostream& s, const Color<T>& c)
    {
        return s << "Color{r: " << c.r
                 << ", g: " << c.g
                 << ", b: " << c.b << '}';
    }
};

using RawColor = Color<unsigned char>;
using CalcColor = Color<double>;

} // namespace raytracer
