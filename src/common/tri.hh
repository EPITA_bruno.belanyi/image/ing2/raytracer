#pragma once

#include <optional>
#include <utility>

#include <cmath>

#include "point.hh"
#include "ray.hh"
#include "vect.hh"

namespace raytracer
{

template<bool ShouldCull>
struct GenericTriImpl
{
    Position c0;
    Vector c0c1;
    Vector c0c2;

    explicit GenericTriImpl() = default;

    explicit GenericTriImpl(Position p0, Position p1, Position p2)
    : c0(p0), c0c1(p1 - p0), c0c2(p2 - p0)
    {}

    std::optional<double>
    intersect(const Ray& ray) const
    {
        const auto& pvec = ray.dir ^ c0c2;
        const auto& det = c0c1 * pvec;
        // if the determinant is negative the triangle is backfacing
        // if the determinant is close to 0, the ray misses the triangle
        if constexpr (ShouldCull)
        {
            if (det < 1e-5)
                return std::nullopt;
        }
        else
        {
            if (std::abs(det) < 1e-5)
                return std::nullopt;
        }

        const auto inv_det = 1 / det;

        const auto& to_ray = ray.pos - c0;
        const auto& u = to_ray * pvec * inv_det;
        if (u < 0 || u > 1)
            return std::nullopt;

        const auto& qvec = to_ray ^ c0c1;
        const auto& v = ray.dir * qvec * inv_det;
        if (v < 0 || u + v > 1)
            return std::nullopt;

        const auto t = c0c2 * qvec * inv_det;
        // Can sometimes be negative because of rounding errors
        if (t > 0)
            return t;
        return std::nullopt;
    }

    Vector normal(const Position&) const
    {
        return (c0c1 ^ c0c2).normalize();
    }

    std::pair<double, double> barycentric(const Position& pos) const
    {
        const auto c0_pos = pos - c0;
        // P - A  = u * (C - A) + v * (B - A)
        // (C - A) = v0 is c0c2
        // (B - A) = v1 is c0c1
        // (P - A) = v2 is c0_pos
        const auto dot00 = c0c2 * c0c2;
        const auto dot01 = c0c2 * c0c1;
        const auto dot02 = c0c2 * c0_pos;
        const auto dot11 = c0c1 * c0c1;
        const auto dot12 = c0c1 * c0_pos;

        const auto inv_denom = 1 / (dot00 * dot11 - dot01 * dot01);
        const auto u = (dot11 * dot02 - dot01 * dot12) * inv_denom;
        const auto v = (dot00 * dot12 - dot01 * dot02) * inv_denom;
        return {u, v};
    }

    bool contains(const Position& pos) const
    {
        // See if it's on the same plane
        if (std::abs(normal(pos) * (pos - c0)) > 1e-3)
            return false;
        const auto [u, v] = barycentric(pos);
        if (u < -1e-3 || u > 1 + 1e-3)
            return false;
        return (v >= -1e-3 && (u + v) <= 1 + 1e-3);
    }
};

template <typename TriImpl>
struct GenericInterTri : public TriImpl
{
    Vector n0;
    Vector n1;
    Vector n2;

    GenericInterTri() = default;

    GenericInterTri(
        Position p0, Position p1, Position p2,
        Vector normal_0, Vector normal_1, Vector normal_2
    ) : TriImpl(p0, p1, p2), n0(normal_0), n1(normal_1), n2(normal_2)
    {}

    Vector normal(const Position& pos) const
    {
        const auto [u, v] = this->barycentric(pos);
        return (n2 * u + n1 * v + n0 * (1 - u -v)).normalize();
    }
};

using NormalTri = GenericTriImpl<false>;
using CullingTri = GenericTriImpl<true>;
using InterpolTri = GenericInterTri<NormalTri>;
using InterpolCullingTri = GenericInterTri<CullingTri>;

} // namespace raytracer
