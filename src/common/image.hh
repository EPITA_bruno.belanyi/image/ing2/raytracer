#pragma once

#include <vector>
#include <iosfwd>

#include <cassert>

#include "color.hh"

namespace raytracer
{

struct Image
{
    Image(unsigned width, unsigned height)
    : w(width), h(height), pixels(width * height)
    {}

    unsigned width() const { return w; };

    unsigned height() const { return h; };

    void put(unsigned width, unsigned height, const RawColor& pixel)
    {
        assert(width < w);
        assert(height < h);

        pixels[width + height * w] = pixel;
    }

    friend std::ostream& operator<<(std::ostream& s, const Image& i)
    {
        s << "P3\n";
        s << i.width() << ' ' << i.height() << '\n';
        s << "255\n";

        unsigned count = 0;
        for (const auto& c : i.pixels)
        {
            if (count == i.width())
            {
                s << '\n';
                count = 0;
            }
            s << static_cast<unsigned>(c.r) << ' '
              << static_cast<unsigned>(c.g) << ' '
              << static_cast<unsigned>(c.b);
            if (++count != i.width())
                s << ' ';
        }

        return s;
    }

private:
    const unsigned w;
    const unsigned h;
    std::vector<Color<unsigned char>> pixels;
};

} // namespace raytracer
