#pragma once

#include <utility>

#include <cassert>
#include <cmath>

#include "point.hh"
#include "vect.hh"

namespace raytracer
{

struct Camera
{
    const Vector u;
    const Vector r;
    const Position p;
    const Position c;
    const double x_size;
    const double y_size;

    Camera(
        Position pos,
        Vector forward,
        Vector up,
        double x_fov,
        double y_fov,
        double dist_to_image
    ) : u(up.normalize())
      , r((forward ^ u).normalize())
      , p(pos)
      , c(pos + forward.normalize() * dist_to_image)
      , x_size(2 * std::tan(x_fov/2) * dist_to_image)
      , y_size(2 * std::tan(y_fov/2) * dist_to_image)
    {}

    Camera(
        Position pos,
        Vector forward,
        Vector up,
        double fov,
        double dist_to_image,
        unsigned x,
        unsigned y
    ) : u(up.normalize())
      , r((forward ^ u).normalize())
      , p(pos)
      , c(pos + forward.normalize() * dist_to_image)
      , x_size(2 * std::tan(fov/2) * dist_to_image * (y > x ? double(x)/y : 1))
      , y_size(2 * std::tan(fov/2) * dist_to_image * (x > y ? double(y)/x : 1))
    {}

    Position get_pixel_pos(double x, double y) const
    {
        // x is column portion, y is row portion
        assert(x >= -0.01 && x <= 1.01);
        assert(y >= -0.01 && y <= 1.01);

        auto x_delt = x_size * (x - 0.5);
        auto y_delt = y_size * (0.5 - y);

        return c + r * x_delt + u * y_delt;
    }
};

} // namespace raytracer
