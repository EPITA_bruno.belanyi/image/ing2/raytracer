#pragma once

#include <iosfwd>
#include <utility>
#include <tuple>
#include <type_traits>

#include <cassert>
#include <cmath>

namespace raytracer
{

template <typename T>
struct Vect
{
    T x;
    T y;
    T z;

    template <typename U>
    friend constexpr Vect<decltype(std::declval<T>() + std::declval<U>())>
    operator+(const Vect<T>& lhs, const Vect<U>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());

        return Vect<Result>{
            lhs.x + rhs.x,
            lhs.y + rhs.y,
            lhs.z + rhs.z,
        };
    }

    template <typename U>
    friend constexpr Vect<decltype(std::declval<T>() - std::declval<U>())>
    operator-(const Vect<T>& lhs, const Vect<U>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());

        return Vect<Result>{
            lhs.x - rhs.x,
            lhs.y - rhs.y,
            lhs.z - rhs.z,
        };
    }

    template <typename U>
    friend constexpr Vect<decltype(std::declval<T>() * std::declval<U>())>
    operator^(const Vect<T>& lhs, const Vect<U>& rhs)
    {
        using Result = decltype(std::declval<T>() * std::declval<U>());

        return Vect<Result>{
            lhs.y * rhs.z - lhs.z * rhs.y,
            lhs.z * rhs.x - lhs.x * rhs.z,
            lhs.x * rhs.y - lhs.y * rhs.x,
        };
    }

    template <typename U>
    friend constexpr decltype(auto)
    operator*(const Vect<T>& lhs, const Vect<U>& rhs)
    {
        return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
    }

    template <typename U>
    friend constexpr std::enable_if_t<
        std::is_arithmetic_v<U>,
        Vect<T>
    >
    operator*(const Vect<T>& lhs, U rhs)
    {
        return Vect<T>{lhs.x * rhs, lhs.y * rhs, lhs.z * rhs};
    }

    template <typename U>
    friend constexpr std::enable_if_t<
        std::is_arithmetic_v<U>,
        Vect<T>
    >
    operator*(U lhs, const Vect<T>& rhs)
    {
        return Vect<T>{rhs.x * lhs, rhs.y * rhs, rhs.z * rhs};
    }

    template <typename U>
    friend constexpr std::enable_if_t<
        std::is_arithmetic_v<U>,
        Vect<T>
    >
    operator/(const Vect<T>& lhs, U rhs)
    {
        return Vect<T>{lhs.x / rhs, lhs.y / rhs, lhs.z / rhs};
    }

    template <typename U>
    friend constexpr std::enable_if_t<
        std::is_arithmetic_v<U>,
        Vect<T>
    >
    operator/(U lhs, const Vect<T>& rhs)
    {
        return Vect<T>{lhs / rhs.x, lhs / rhs.y, lhs / rhs.z};
    }

    friend Vect<T>
    operator/(const Vect<T>& lhs, const Vect<T>& rhs)
    {
        return Vect<T>{lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z};
    }

    friend std::ostream& operator<<(std::ostream& s, const Vect<T>& v)
    {
        return s << "Vect{x: " << v.x
                 << ", y: " << v.y
                 << ", z: " << v.z << '}';
    }

    double norm() const
    {
        return std::sqrt(x * x + y * y + z * z);
    }

    Vect<T>& normalize()
    {
        const auto n = norm();
        x /= n;
        y /= n;
        z /= n;
        return *this;
    }

    friend bool operator<(const Vect<T>& lhs, const Vect<T>& rhs)
    {
        return std::tie(lhs.x, lhs.y, lhs.z) < std::tie(rhs.x, rhs.y, rhs.z);
    }
};

using Vector = Vect<double>;

} // namespace raytracer
