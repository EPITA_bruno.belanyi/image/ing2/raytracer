#pragma once

#include "point.hh"

namespace raytracer
{

struct AABB
{
    Position l =
    {
        std::numeric_limits<double>::max(),
        std::numeric_limits<double>::max(),
        std::numeric_limits<double>::max(),
    };
    Position u =
    {
        std::numeric_limits<double>::min(),
        std::numeric_limits<double>::min(),
        std::numeric_limits<double>::min(),
    };

    explicit AABB() = default;

    AABB(Position lower_bound, Position upper_bound)
    : l(lower_bound), u(upper_bound)
    {}

    AABB& extend(const Position& p)
    {
        // Extend lower-bound
        l.x = std::min(p.x, l.x);
        l.y = std::min(p.y, l.y);
        l.z = std::min(p.z, l.z);
        // Extend upper-bound
        u.x = std::max(p.x, u.x);
        u.y = std::max(p.y, u.y);
        u.z = std::max(p.z, u.z);
        return *this;
    }

    AABB& operator|=(const AABB& rhs)
    {
        // Extend lower-bound
        l.x = std::min(l.x, rhs.l.x);
        l.y = std::min(l.y, rhs.l.y);
        l.z = std::min(l.z, rhs.l.z);
        // Extend upper-bound
        u.x = std::max(u.x, rhs.u.x);
        u.y = std::max(u.y, rhs.u.y);
        u.z = std::max(u.z, rhs.u.z);
        return *this;
    };

    friend AABB operator|(const AABB& lhs, const AABB& rhs)
    {
        auto res = lhs;
        res |= rhs;
        return res;
    }

    bool contains(const Position& pos) const
    {
        return l.x <= pos.x && pos.x <= u.x
            && l.y <= pos.y && pos.y <= u.y
            && l.z <= pos.z && pos.z <= u.z;
    }

    Vector diagonal() const
    {
        return u - l;
    }

    Position centroid() const
    {
        return l + diagonal() / 2;
    }

    bool empty() const
    {
        return l.x > u.x || l.y > u.y || l.z > u.z;
    }
};

} // namespace raytracer
